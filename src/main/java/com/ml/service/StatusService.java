package com.ml.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ml.model.Dna;
import com.ml.DTO.StatusGenesDTO;
import com.ml.repository.SimianRepository;

@Service
public class StatusService {

	@Autowired
	private SimianRepository simianRepository;

	public StatusGenesDTO checkGenesStatus() {
		StatusGenesDTO statusGenesDTO = new StatusGenesDTO();
		List<Dna> dnaList = getAllDna();
		//Check occurrence of Simian Dna
		if (!dnaList.isEmpty()) {
			statusGenesDTO.setTotalSimianDna(dnaList.stream().filter(dna -> dna.getSimianGenesSequences() >= 2).count());
			//There's only 2 types of Dna
			statusGenesDTO.setTotalHumanDna(dnaList.size() - statusGenesDTO.getTotalSimianDna());
			//Calculates ratio of difference kinds of DNA (if no human DNA ratio = 0)
			if (statusGenesDTO.getTotalHumanDna() != 0)
				statusGenesDTO.setRatio(((float) statusGenesDTO.getTotalSimianDna() / (float) statusGenesDTO.getTotalHumanDna()));
		}
		return statusGenesDTO;
	}

	public List<Dna> getAllDna() {
		return StreamSupport.stream(simianRepository.findAll().spliterator(), false)
				.collect(Collectors.toList());
	}
}
