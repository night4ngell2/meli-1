package com.ml.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ml.model.Dna;

@Repository
public interface SimianRepository extends CrudRepository<Dna, Long>{

	public Dna findByDnaFullSequence(String dnaFullSequence);
}
