package com.ml.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ml.DTO.SimianRequestDTO;
import com.ml.DTO.StatusGenesDTO;
import com.ml.service.SimianService;
import com.ml.service.StatusService;

@Controller
public class DnaController {

	@Autowired
	private SimianService simianService;
	
	@Autowired
	private StatusService statusService;
	
	@PostMapping("/simian")
	public ResponseEntity<Object> checkSimian(@RequestBody SimianRequestDTO dna) {
		String [] dnaSequence = dna.getDna();
		
		if(simianService.checkSimian(dnaSequence)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
	}
	
	@GetMapping("/stats")
	public ResponseEntity<StatusGenesDTO> checkStats() {
		 return new ResponseEntity<>(statusService.checkGenesStatus(), HttpStatus.OK);
	}
}
