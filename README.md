Endpoint: MeliSimios-env.2iqzswp36z.us-east-1.elasticbeanstalk.com

Operações:

	/simian [POST]:
		Recebe um array de String com a propriedade "dna" via JSON. Verifica se um DNA pertence a um símio, se encontrar mais de uma sequência de quatro letras iguais
nas direções horizontais, verticais ou nas diagonais.
		O array deve ser NxN e conter somente as letras A, T, C, G.
		O array deve ser único (DB não aceita registros repetidos)
		
	Ex:
	{
	"dna":	["AAAAT", 
			"TAAAT",
			"ATATT",
			"TBTAA",
			"ACCTA"]
	}
	/stats [GET]
		Informa a quantidade de DNA's previamente enviados e salvos em DB. 
		Retorna um JSON informando a quantidade de DNA's mutantes (símios), DNA's humanos (não-símios) e a proporção de mutantes para humanos.
		Ex:
		{"count_mutant_dna": 40, "count_human_dna": 100: "ratio": 0.4}
		
Obs: as iterações não foram cortadas quando encontrados 2 sequencias (o que já identifica um simio) para salvar todos encontrados para um "possível" uso posterior. (supondo que a regra de négocio viesse a mudar)